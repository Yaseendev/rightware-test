import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'RestaurantsApi_helper.dart';
import 'app_locale.dart';

final primaryLangProvider = StateProvider<bool>((ref) => false);

final appLocaleProvider = Provider<AppLocale>((ref) => AppLocale());

final appLocaleStateProvider =
    StateNotifierProvider<AppLocaleNotifier, bool>((ref) {
  return AppLocaleNotifier(true);
});

final darkModeStateProvider = StateProvider<bool>((ref) => false);

final listFutureProvider = FutureProvider.autoDispose(
    (ref) => ResturantsApiHelper().getRestaurantsList());

final selectedIndexProvider = StateProvider<int>((ref)=>0);