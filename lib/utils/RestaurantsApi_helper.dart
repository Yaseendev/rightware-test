import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:oriano_list/models/restaurant_model.dart';

class ResturantsApiHelper {
  static const String apiUrl =
      'https://api.orianosy.com/shop/test/find/all/shop';
  static const String secretKey = '2a2xExPa!&+bO9KS!aMC';
  Dio _dio = Dio();

  Future<List<Restaurant>> getRestaurantsList() async {
    _dio.options.headers.putIfAbsent('secretKey', () => secretKey);
    final String deviceType=getDeviceType();
    Response response = await _dio.get('$apiUrl?deviceKind=$deviceType');
    if (response.statusCode != 200) {
      print(response.statusCode.toString() + " unable to fetch weather data");
    }
    final List<Restaurant> restaurants = [];
    for (var restaurant in response.data['result']) {
      restaurants.add(Restaurant.fromJson(restaurant));
    }
    return restaurants;
  }

  String getDeviceType() {
    if (Platform.isAndroid) return 'android';
    if (Platform.isIOS) return 'ios';
    if (Platform.isWindows) return 'windows';
    if (Platform.isMacOS) return 'macos';
    if (kIsWeb) return 'web browser';
    return 'other';
  }
}
