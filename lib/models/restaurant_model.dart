class Restaurant {
  final String? id;
  final String? arName;
  final String? enName;
  final String? arDescription;
  final String? enDescription;
  final int? minimumOrder;
  final String? deleveryTime;
  final String? address;
  final String? photoUrl;

  Restaurant(
      {this.id,
      this.enName,
      this.arName,
      this.enDescription,
      this.arDescription,
      this.address,
      this.minimumOrder,
      this.deleveryTime,
      this.photoUrl});

  Restaurant.fromJson(Map<String, dynamic> parsedJson)
      : id = parsedJson['_id'],
        enName = parsedJson['shopName']['en'],
        arName = parsedJson['shopName']['ar'],
        enDescription = parsedJson['description']['en'],
        arDescription = parsedJson['description']['ar'],
        address = parsedJson['address']['city'] +
            ', ' +
            parsedJson['address']['state'] +
            ', ' +
            parsedJson['address']['country'] +
            ' ' +
            parsedJson['address']['street'] +
            ', ' +
            parsedJson['address']['otherDetails'],
        minimumOrder = parsedJson['minimumOrder']['amount'],
        deleveryTime =parsedJson['estimatedDeliveryTime'],
        photoUrl = parsedJson['coverPhoto'];
}
