import 'package:flutter/material.dart';
import 'package:oriano_list/models/restaurant_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:oriano_list/utils/providers.dart';
import 'package:flutter_gen/gen_l10n/app_localization.dart';

class RestaurantCard extends StatelessWidget {
  final Restaurant restaurant;
  RestaurantCard(this.restaurant);
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Card(
      child: ListTile(
        leading: Container(
            width: width * .25,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(restaurant.photoUrl!)))),
        title: context.read(appLocaleStateProvider) ? Text(restaurant.enName!) : Text(restaurant.arName!),
        subtitle: context.read(appLocaleStateProvider) ? Text(
            '${restaurant.enDescription}\nDelivery Time: ${restaurant.deleveryTime}\nMinimum order: ${restaurant.minimumOrder}\nLocation: ${restaurant.address}') 
            : Text(
            '${restaurant.arDescription}\n${AppLocalizations.of(context)!.deliveryTime}: ${restaurant.deleveryTime}\n${AppLocalizations.of(context)!.minOrder}: ${restaurant.minimumOrder}\n${AppLocalizations.of(context)!.location}: ${restaurant.address}'),
      ),
    );
  }
}
