import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:oriano_list/utils/providers.dart';

class AppBarSwitch extends StatelessWidget {
  final String? leading;
  final String? trailing;
  final Function(bool)? onSwitch;
  final dynamic val;
  AppBarSwitch({this.leading, this.trailing,this.val, this.onSwitch});
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      final lang = watch(primaryLangProvider);
      final _appLocaleStateProvider =
          context.read(appLocaleStateProvider.notifier);
      final _appLocaleState = watch(appLocaleStateProvider);
      return Row(
        children: [
          Text(leading!),
          Switch(
              value:val,
              onChanged: onSwitch!,
              ),
          Text(trailing!),
        ],
      );
    });
  }
}
