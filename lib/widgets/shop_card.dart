import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:oriano_list/models/restaurant_model.dart';
import 'package:oriano_list/utils/providers.dart';
import 'package:flutter_gen/gen_l10n/app_localization.dart';

class ShopCard extends ConsumerWidget {
  final Restaurant restaurant;
  ShopCard(this.restaurant);
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    final darkModeEnabled = watch(darkModeStateProvider);
    return Container(
      margin: EdgeInsets.only(
        left: 8.0,
        right: 8,
        bottom: 10,
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(restaurant.photoUrl!),
            fit: BoxFit.cover,
          ),
          color: Colors.grey[500],
          borderRadius: BorderRadius.all(Radius.circular(30))),
      height: width * .55,
      width: width,
      child: BlurryContainer(
        bgColor: darkModeEnabled.state ?  Colors.black : Colors.grey,
        child: Column(
          children: [
            Container(
              width: width * .22,
              child: FittedBox(
                child: Text(
                  context.read(appLocaleStateProvider) ? restaurant.enName! : restaurant.arName!,
                  style: GoogleFonts.pacifico().copyWith(fontSize: 30),
                ),
              ),
            ),
            Text(
              context.read(appLocaleStateProvider) ? restaurant.enDescription! : restaurant.arDescription!,
              style: TextStyle(fontSize: 15),
              textAlign: TextAlign.center,
            ),
            SizedBox(),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(AppLocalizations.of(context)!.minOrder),
                      Container(
                          width: width * .2,
                          child: FittedBox(
                              child: Text(restaurant.minimumOrder.toString()))),
                    ],
                  ),
                  Center(
                      child: Container(
                    width: ScreenUtil().setWidth(3),
                    height: height * .07,
                    color: darkModeEnabled.state ? Colors.white : Colors.black,
                  )),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(AppLocalizations.of(context)!.deliveryTime),
                      Container(
                          width: width * .2,
                          child:
                              FittedBox(child: Text(restaurant.deleveryTime!))),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                restaurant.address!,
                style: TextStyle(fontSize: 12),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
