import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:oriano_list/models/restaurant_model.dart';
import 'package:oriano_list/utils/providers.dart';
import 'package:flutter_gen/gen_l10n/app_localization.dart';
import 'package:oriano_list/widgets/language_switch.dart';
import 'package:oriano_list/widgets/shop_card.dart';
import 'package:shimmer/shimmer.dart';

final _searchTermProvider = StateProvider<String>((ref) => '');
List<Restaurant> _resultList = [];

class ShopListSecondScreen extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final _appLocaleState = watch(appLocaleStateProvider);
    final _appLocaleStateProvider =
        context.read(appLocaleStateProvider.notifier);
    final shopsList = watch(listFutureProvider);
    double width = MediaQuery.of(context).size.width;
    final _searchTerm = watch(_searchTermProvider);
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                AppBarSwitch(
                  leading: 'AR',
                  trailing: 'EN',
                  val: _appLocaleState,
                  onSwitch: (selectedLang) =>
                      _appLocaleStateProvider.toggleAppLocale(selectedLang),
                ),
                Text(
                  AppLocalizations.of(context)!.appBarTitle,
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
                ),
                AppBarSwitch(
                  leading: AppLocalizations.of(context)!.light,
                  trailing: AppLocalizations.of(context)!.dark,
                  val: context.read(darkModeStateProvider).state,
                  onSwitch: (theme) =>
                      context.read(darkModeStateProvider).state = theme,
                ),
              ],
            ),
            shopsList.when(
                data: (data) => Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Flexible(
                          fit: FlexFit.loose,
                          child: TextField(
                            autofocus: false,
                            decoration: InputDecoration(
                                icon: Icon(Icons.search), hintText: 'Search'),
                            onChanged: (searchedName) {
                              _searchTerm.state = searchedName;
                              _appLocaleState
                                  ? _resultList = data
                                      .where((element) => element.enName!
                                          .contains(_searchTerm.state
                                              .replaceFirst(
                                                  RegExp(_searchTerm.state[0]),
                                                  _searchTerm.state[0]
                                                      .toUpperCase())))
                                      .toList()
                                  : _resultList = data
                                      .where((element) => element.arName!
                                          .contains(_searchTerm.state))
                                      .toList();
                            },
                          ),
                        ),
                        _searchTerm.state == ''
                            ? Flexible(
                                fit: FlexFit.loose,
                                child: ListView.builder(
                                  padding: const EdgeInsets.all(10),
                                  physics:
                                      const AlwaysScrollableScrollPhysics(),
                                  scrollDirection: Axis.vertical,
                                  itemCount: data.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index) =>
                                      ShopCard(data[index]),
                                ),
                              )
                            : Flexible(
                                fit: FlexFit.loose,
                                child: ListView.builder(
                                  padding: const EdgeInsets.all(8),
                                  shrinkWrap: true,
                                  itemCount: _resultList.length,
                                  itemBuilder: (context, index) =>
                                      ShopCard(_resultList[index]),
                                ),
                              ),
                      ],
                    ),
                loading: () => Shimmer.fromColors(
                    baseColor: Colors.grey[300]!,
                    highlightColor: Colors.grey[100]!,
                    child: Expanded(
                      child: ListView(
                          padding: const EdgeInsets.all(10),
                          physics: const AlwaysScrollableScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          children: List.generate(
                              2,
                              (index) => Container(
                                    margin: EdgeInsets.only(
                                      left: 8.0,
                                      right: 8,
                                      bottom: 2,
                                    ),
                                    decoration: BoxDecoration(
                                        color: Colors.grey[500],
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30))),
                                    height: width * .55,
                                    width: width,
                                  ))),
                    )),
                error: (error, st) => Center(
                    child: Text(
                        'Could not fetch data..Check your network connection'))),
          ],
        ),
      ),
    );
  }
}
