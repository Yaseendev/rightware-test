import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:oriano_list/screens/shops_list1_screen.dart';
import 'package:oriano_list/utils/providers.dart';

import 'shops_list2_screen.dart';

class Home extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final _selectedIndex = watch(selectedIndexProvider);
    final List<Widget> screens = [
      ShopListFirstScreen(),
      ShopListSecondScreen(),
    ];
    return Scaffold(
      body: screens.elementAt(_selectedIndex.state),
      bottomNavigationBar: BottomNavigationBar(items: const [
      BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'List Design 1',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'List Design 2',
          ),
    ],
    currentIndex: _selectedIndex.state,
    onTap: (index)=> context.read(selectedIndexProvider).state = index,
    ),);
  }
}
