import 'package:flutter/material.dart';
import 'package:oriano_list/models/restaurant_model.dart';
import 'package:oriano_list/utils/RestaurantsApi_helper.dart';
import 'package:oriano_list/utils/providers.dart';
import 'package:oriano_list/widgets/language_switch.dart';
import 'package:oriano_list/widgets/restaurant_card.dart';
import 'package:flutter_gen/gen_l10n/app_localization.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shimmer/shimmer.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';

final _searchTermProvider = StateProvider<String>((ref) => '');
List<Restaurant> _resultList = [];

class ShopListFirstScreen extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    double width = MediaQuery.of(context).size.width;
    final _appLocaleState = watch(appLocaleStateProvider);
    final _appLocaleStateProvider =
        context.read(appLocaleStateProvider.notifier);
    final shopsList = watch(listFutureProvider);
    final _searchTerm = watch(_searchTermProvider);
    return Scaffold(
        appBar: AppBar(
          leading: AppBarSwitch(
            leading: 'AR',
            trailing: 'EN',
            val: _appLocaleState,
            onSwitch: (selectedLang) =>
                _appLocaleStateProvider.toggleAppLocale(selectedLang),
          ),
          title: Text(AppLocalizations.of(context)!.appBarTitle),
          centerTitle: true,
          leadingWidth: width * .25,
          actions: [
            AppBarSwitch(
              leading: AppLocalizations.of(context)!.light,
              trailing: AppLocalizations.of(context)!.dark,
              val: context.read(darkModeStateProvider).state,
              onSwitch: (theme) =>
                  context.read(darkModeStateProvider).state = theme,
            ),
          ],
        ),
        body: shopsList.when(
            data: (data) => Column(
                  children: [
                    TextField(
                      autofocus: false,
                      decoration: InputDecoration(
                          icon: Icon(Icons.search), hintText: 'Search'),
                      onChanged: (searchedName) {
                        _searchTerm.state = searchedName;
                        _appLocaleState
                            ? _resultList = data
                                .where((element) => element.enName!.contains(
                                    _searchTerm.state.replaceFirst(
                                        RegExp(_searchTerm.state[0]),
                                        _searchTerm.state[0].toUpperCase())))
                                .toList()
                            : _resultList = data
                                .where((element) =>
                                    element.arName!.contains(_searchTerm.state))
                                .toList();
                      },
                    ),
                    _searchTerm.state == ''
                        ? Expanded(
                            child: ListView.builder(
                              padding: const EdgeInsets.all(8),
                              shrinkWrap: true,
                              itemCount: data.length,
                              itemBuilder: (context, index) =>
                                  RestaurantCard(data[index]),
                            ),
                          )
                        : Expanded(
                            child: ListView.builder(
                              padding: const EdgeInsets.all(8),
                              shrinkWrap: true,
                              itemCount: _resultList.length,
                              itemBuilder: (context, index) =>
                                  RestaurantCard(_resultList[index]),
                            ),
                          ),
                  ],
                ),
            loading: () => Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: ListView(
                  children: List.generate(
                    3,
                    (index) => ListTile(
                      leading: Container(
                        width: width * .25,
                        color: Colors.grey,
                      ),
                      title: Container(
                        width: width * .1,
                        height: width * .06,
                        color: Colors.grey,
                      ),
                      subtitle: Container(
                        width: width * .1,
                        height: width * .17,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                )),
            error: (error, st) => Center(
                child: Text(
                    'Could not fetch data..Check your network connection')))
        );
  }
}
