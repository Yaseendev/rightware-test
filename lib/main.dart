import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'screens/home_screen.dart';
import 'utils/constans.dart';
import 'screens/shops_list1_screen.dart';
import 'utils/RestaurantsApi_helper.dart';
import 'utils/l10n/l10n.dart';
import 'package:flutter_gen/gen_l10n/app_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'utils/providers.dart';

void main() {
  ResturantsApiHelper helper = ResturantsApiHelper();
  helper.getRestaurantsList();
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final _appLocaleState = watch(appLocaleStateProvider);
    final darkModeEnabled = watch(darkModeStateProvider);
    return ScreenUtilInit(
        designSize: Size(1080, 1920),
        builder: () => MaterialApp(
              title: 'Oriano App',
              supportedLocales: L10n.all,
              localizationsDelegates: [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              locale: context
                  .read(appLocaleProvider)
                  .getAppLocale(context, _appLocaleState),
                theme: darkModeEnabled.state ? ThemeData.dark() : ThemeData.light(),
              debugShowCheckedModeBanner: false,
              home: Home(),
            ));
  }
}
